
# Copyright (C) 2004-2013  Moreno 'baro' Baricevic
#                        <baro AT democritos DOT it>

INSTALL = install -v -D -p -o root -g root

qusage:
	make -C src

clean:
	make -C src clean

install: install-qusage

install-all: install-qusage install-quacct install-quacct-cron

install-qusage: qusage
	awk 'NF==3 && $$1 ~ /^qusage$$/		{print $$2,$$3}' install.rc | xargs -t -n2 $(INSTALL) -m 755
	awk 'NF==3 && $$1 ~ /^qusage-man$$/	{print $$2,$$3}' install.rc | xargs -t -n2 $(INSTALL) -m 644

install-quacct:
	awk 'NF==3 && $$1 ~ /^quacct$$/		{print $$2,$$3}' install.rc | xargs -t -n2 $(INSTALL) -m 755

install-quacct-cron:
	awk 'NF==3 && $$1 ~ /^quacct-cron$$/	{print $$2,$$3}' install.rc | xargs -t -n2 $(INSTALL) -m 755

uninstall:
	@awk 'NF==3 && $$0 !~ /^#/ {print $$2,$$3}' install.rc |		\
		while read src dst ; do						\
			test -e $$dst || continue ;				\
			diff -q $$src $$dst					\
				&& rm -fv $$dst					\
				|| echo >&2 "skipping removal of $$dst" ;	\
		done

#EOF
