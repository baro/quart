#!/bin/bash

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Path: /etc/accounting/bin           #
# File: quacctget_user_limit.sh       #
# Date: 28 Aug 2004                   #
#-------------------------------------#
# Prev Modified: 28 Aug 2004          #
# Last Modified: 12 Sep 2004          #
#=====================================#

QUOTAFILE=/etc/accounting/accounting.users

function bell()
{
   [ -t 2 ] && echo -ne >&2 "\a"
}

function usage()
{
   test $# != 0  && { bell ; echo >&2 "*** usage error: $*" ; }
   echo >&2 "Usage: $0 USER"
   exit $((!!$#))
}

test -z "$1" && usage "empty username"

user="$1"

line="$(grep "^$user:" $QUOTAFILE)"

test -z "$line" && {
   bell
   echo >&2 "*** user '$user' doesn't exist on quotafile '$QUOTAFILE'"
   exit 1
}

echo $line | awk -F: '
{

   printf( "\n"	"line = [%s]\n" , $0 );

   printf(	"\n"				\
		"user:  \t%s\n"			\
		"limit: \t%-6s (hours)\n"	\
		"usage: \t%-6s (hours)\n"	\
		"status:\t%s\n"			\
		"\n" ,
		$1 ,
		$2 ,
		$3 ,
		( $4 == 1 ) ? "DISABLED" : "ACTIVE" );

}
'
exit 0

#EOF
