#!/usr/bin/perl -w

###############################################################################
##########################  subroutines  description ##########################
###############################################################################
#
# MAIN: {};		Container...    ];->
#			=> subs parse_options, get_quota, get_usage,
#			   store_quota, update_quota.
#
# usage {};		display a short usage/help message
#			=> subs .
#
# parse_options {};	cmdline options parsing
#			=> subs usage.
#
# get_usage {};		get yesterday's usage statistitic (invokes qusage)
#			=> subs .
#
# get_quota {};		parse accounting quota file and does a lot of funny
#			things (look at the subroutine comments)
#			=> subs check_fields, user_pbs, user_account,
#			   set_user_status.
#
# store_quota {};	write a temporary quota file with the updated usage
#			statistics
#			=> subs check_fields.
#
# update_quota {};	move (old)quotafile to backup and temporary file to
#			(new)quotafile.
#			=> subs .
#
# check_fields {};	just a sanity check for acquired quotafile fields
#			and values.
#			=> subs .
#
# notify_user {};	warn admin and user by e-mail
#			=> subs .
#
# user_pbs {};		disable/enable the user's pbs-submission privilege
#			=> subs notify_user.
#
# user_account {};	lock the user's account
#			=> subs notify_user.
#
# set_user_status {};	change the 'disabled' user's field on the current
#			quotafile
#			=> subs lock, unlock.
#
# lock {};		set exclusive writing lock on quotafile
#			=> subs .
#
# unlock {};		unlock quotafile
#			=> subs .
#
###############################################################################
########################## /subroutines  description ##########################
###############################################################################


#-----------------------------------#
# Author: Moreno 'baro' Baricevic   #
# Path: /etc/accounting/bin         #
# File: quacctquota.pl              #
# Date: 09 Aug 2004                 #
# Platform: Linux                   #
#-----------------------------------#
# Prev modified:  09 Aug 2004       #
# Prev modified:  10 Aug 2004       #
# Prev modified:  11 Aug 2004       #
# Prev modified:  12 Aug 2004       #
# Prev modified:  13 Aug 2004       #
# Prev modified:  18 Aug 2004       #
# Prev modified:  26 Aug 2004       #
# Prev modified:  27 Aug 2004       #
# Prev modified:  29 Aug 2004       #
# Prev modified:  07 Oct 2004       #
# Last modified:  08 Oct 2004       #
#-----------------------------------#

require 5.003;
use strict;
use Getopt::Std;
use POSIX qw( strftime getuid );

die "Sorry, you must be root.\n" unless getuid() == 0;

#--------------------------------------------------------------------
#
# environment settings
#
$| = 1;       # Always auto-flush stdout.

#--------------------------------------------------------------------
#
# this program
#
my $PROGRAM_NAME   = "quacctquota.pl";
my $VERSION        = "beta1.0";
my $AUTHOR         = "Moreno `Baro' Baricevic";
my $AUTHOR_CONTACT = '<baro@democritos.it>';

#--------------------------------------------------------------------
#
# date for 'chage' expiration-date and pbs accounting-file
#

use constant CHAGE_FMT => "%Y-%m-%d" ;	# date format used by chage (expire date)
use constant PBS_FMT   => "%Y%m%d" ;	# date format used by pbs (acct file)

my $EXPIRENOW = strftime( CHAGE_FMT , localtime( time ) );
my $EXPIREGRC = strftime( CHAGE_FMT , localtime( time + (86400*180) ) ); # 6 months grace
my $YESTERDAY = strftime( PBS_FMT   , localtime( time - 86400 ) );

#--------------------------------------------------------------------
#
# cmdline options
#
my $UPDATE  = 0;	# get new usage and store usage value
my $FORCE   = 0;	# force lock/unlock overquota users
my $QUIET   = 0;	# (unimplemented)
my $DEBUG   = 0;	# enable some debug prints...
my $VERBOSE = 0;	# (unimplemented)

my $INTERACTIVE = ( -t STDOUT && -t STDERR );   # TRUE if term, FALSE if cron

#--------------------------------------------------------------------
#
# ACCT QUOTA FILE
#
my $ROOTFS = "/" ;
my $QUOTAFILE = $ROOTFS . "/etc/accounting/accounting.users" ;
my $BCKFILE   = undef;

#--------------------------------------------------------------------
#
# PBS ACCOUNTING FILES and QUSAGE executable and cmdline options
#
my $ACCTPATH = $ROOTFS . "/var/spool/PBS/server_priv/accounting" ;
my $ACCTFILE = $ACCTPATH . '/' . $YESTERDAY ;

my $QUSAGE = $ROOTFS . "/etc/accounting/bin/qusage" ;
my $QUSAGE_OPTIONS = "--show-only-users -q -S :" ;

# little sanity check, we shouldn't work without qusage.
my $exe = ( $QUSAGE =~ m|/| ) ? $QUSAGE : `which $QUSAGE 2>/dev/null` ;
die "$QUSAGE is not executable!\n" if ( not $exe ) or ( not -x $exe );

#--------------------------------------------------------------------
#
# mail vars (destination mailhost, admin account) and local hostname
#
my $MAILHOST = 'localhost' ;
my $ADMIN    = 'root@' . $MAILHOST ;
my $HOSTNAME = `hostname`;
chomp( $HOSTNAME );
my $ADMINCONTACT = 'cluster-admin@yourdomain.com';

#--------------------------------------------------------------------
#
# global dump structure
#
my $dump = {
	users => [ ] ,
};

# my $test = {
#	array   => [ ],
#	hash    => { },
#	nothing => ( ),
#};
#   warn "a " , ref $dump->{array} , "\n";
#   warn "h " , ref $dump->{hash} , "\n";
#   warn "n " , ref $dump->{nothing} , "\n";

#--------------------------------------------------------------------
#
# some constants
#
# (!0) IS (1)
# (!1) IS NOT (0) !!!!!!!!
#

use constant QUIET   => 0;
use constant WARN    => (!QUIET);

use constant ENABLE  => 0;
use constant DISABLE => (!ENABLE);

use constant FAILURE => 0;
use constant SUCCESS => (!FAILURE);

#--------------------------------------------------------------------
#
# options
#
my %opt;
my $stdopt = 'hvqd';		# standard options: help,verbose,quiet,debug,..
my $developt = 'fuA:Q:X:' ;	# -f, -u, -A ACCTFILE, -Q QUOTAFILE, -X QUSAGE


my @QUOTA = ();

### END GLOBAL VARIABLES DECLARATION ###

#=-  -=#
# MAIN #
#=-  -=#
MAIN:
{

   parse_options();

   acquire( \@QUOTA , $QUOTAFILE );

   if ( $UPDATE )	# if -u
   {
      # look at the quotafile content and initialize dump->[users} struct
      get_quota( \@QUOTA , QUIET );

      # get yesterday's usage
      get_usage( $ACCTFILE );

      # add new usage to the quotafile content
      update_quota( \@QUOTA );
   }

   # look at the quotafile content and, if -f, disable (or re-enable) overquota-users
   get_quota( \@QUOTA , WARN );

   # store new usage statistics to quotafile and backup the old one
   store( \@QUOTA , $QUOTAFILE , $BCKFILE ) if $UPDATE or $FORCE ;

} # MAIN #



####################################################################
#_____________________________  USAGE _____________________________#
####################################################################
#
# script usage...
#
sub usage
{
   my $errmsg = shift;
   print "*** $errmsg\n" if defined $errmsg;

   print <<EOF;

Usage: $0 [-u] [-f] [-A ACCTFILE] [-Q QUOTAFILE] [-X QUSAGE] [-q] [-v] [-d] [-h]

	-u		get new usage statistics and update quotafile
	-f		force user account (un)locking

	-A ACCTFILE	use alternative pbs-accounting file
	-Q QUOTAFILE	use alternative accounting quota file
	-X QUSAGE	use alternative path to qusage

	-q		quiet output (not yet implemented)
	-v		verbose output (not yet implemented)
	-d		enable debug messages
	-h		show this message

defaults:
	ACCTFILE  = $ACCTFILE
	QUOTAFILE = $QUOTAFILE
	QUSAGE    = $QUSAGE

EOF

   return( defined $errmsg );

} # usage #



######################################################################
#_____________________________  ACQUIRE _____________________________#
######################################################################
#
# acquire quotafile content into given array ref
#
sub acquire
{
   my $array  = shift;
   my $file   = shift;
   open QUOTA , $file or die "*** cannot open quota file $file: $!\n";
   @{$array} = <QUOTA>;
   close QUOTA;
} # acquire #



####################################################################
#_____________________________  STORE _____________________________#
####################################################################
#
# store given array content to quotafile
#
sub store
{
   my $array  = shift;
   my $file   = shift;
   my $backup = shift;
   rename $file , $backup or die "*** cannot backup $file to $backup: $!\n";
   open  QUOTA , ">$file" or die "*** cannot open quota file $file: $!\n";
   print QUOTA join "\n" , @{$array};
   close QUOTA;
} # store #



############################################################################
#_____________________________  PARSE_OPTIONS _____________________________#
############################################################################
#
# cmdline options parsing
#
sub parse_options
{
   my $getopt_ok = getopts( $stdopt . $developt , \%opt );
   if ( ! $getopt_ok )
   {
      usage();
      exit 1;
   }
   die "Usage error: junk on cmdline\n" if ( @ARGV );

   exit usage() if exists $opt{h};

   $UPDATE  = ( exists $opt{u} );
   $FORCE   = ( exists $opt{f} );

   $QUIET   = ( exists $opt{q} );
   $DEBUG   = ( exists $opt{d} );
   $VERBOSE = ( exists $opt{v} );

   if ( exists $opt{A} and $opt{A} )
   {
      $ACCTFILE  = $opt{A} ;
      exit usage( "acctfile '$ACCTFILE' is not a file or is unreadable" ) unless -f $ACCTFILE and -r _ ;
   }

   if ( exists $opt{X} and $opt{X} )
   {
      $QUSAGE    = $opt{X};
      exit usage( "qusage '$QUSAGE' is not executable" ) unless -x $QUSAGE;
   }

   if ( exists $opt{Q} and $opt{Q} )
   {
      my $quotadir = "./";
      $QUOTAFILE = $opt{Q};
      exit usage( "quotafile '$QUOTAFILE' is not a file or is unreadable" ) unless -f $QUOTAFILE and -r _ ;
      $quotadir = $1 if $QUOTAFILE =~ m|^(.+)/[^/]+$|;
      exit usage( "'$quotadir' is not a directory or has wrong permissions" ) unless -d $quotadir and -r _ and -w _ and -x _ ;
   }
   $BCKFILE = $QUOTAFILE . "._BCK_";

} # parse_options #



########################################################################
#_____________________________  GET_USAGE _____________________________#
########################################################################
#
# Invoke qusage against yesterday's accounting file to get the new
# daily usage.
#
sub get_usage
{
   my $file = shift;

   my @usage = `$QUSAGE $QUSAGE_OPTIONS $file 2>&1`;
   die "*** qusage error: @usage\n" if ( $? >> 8 );

   foreach ( @usage )
   {
      chomp;
      next unless /^[^:]+:[^:]+:[^:]+:[^:]+:[^:]+$/;	# /^([^:]+:){4,4}[^:]+$/;
      my ( $user , $group , $jobs , $usage , $usage_nodes ) = split /:/ , $_;
      $dump->{$user}->{usage} = $usage_nodes;
   }

} # get_usage #



########################################################################
#_____________________________  GET_QUOTA _____________________________#
########################################################################
#
# Parse accounting quota file and (if QUIET) just initialize the
# $dump->{users} structure or (if WARN) disable any user in overquota
# or re-enable a user's account if the limit has been manually
# increased ('limit' < 'usage' but 'disabled' field set).
# It delegates to set_user_status() the task of setting the 'disabled'
# field.
#
sub get_quota
{

   my $array = shift;
   my $warn  = shift;

   foreach ( @{$array} )
   {
      chomp;

      next if /^[ \t]*$/;
      next if /^[ \t]*#/;

      my ( $user , $limit , $usage , $disabled , $foo ) = check_fields( $_ );

      if ( $warn )
      {
         my $line = $_;

         if ( $limit == -1 )
         {
            if ( $disabled )
            {
               warn "use -f option to force user unlocking\n" and next unless $FORCE;
               user_pbs( $user , ENABLE );
#               user_account( $user , DISABLE );
               set_user_status( \$_ , ENABLE , $line );
            }
            else {}	# nothing to do, unlimited usage
         }
         elsif ( $usage > $limit )
         {
            # warn and disable PBS job submission (or lock the account)
            warn "overquota for user '$user': $usage of $limit\n" ;
            warn "user '$user' already disabled\n" and next if $disabled;

            printf( "line: %s\n" , $line ) if $DEBUG;

            warn "use -f option to force user locking\n" and next unless $FORCE;

            user_pbs( $user , DISABLE );
#            user_account( $user , DISABLE );
            set_user_status( \$_ , DISABLE , $line );
         }
         elsif ( $disabled && $usage < $limit )
         {
            warn "use -f option to force user unlocking\n" and next unless $FORCE;

            user_pbs( $user , ENABLE );
#            user_account( $user , ENABLE );
            set_user_status( \$_ , ENABLE , $line );
         }
      }
      else	# quiet, just init users array for next checks
      {
         push( @{$dump->{users}} , $user );
      }

   } # foreach

} # get_quota #



###########################################################################
#_____________________________  UPDATE_QUOTA _____________________________#
###########################################################################
#
# Update quota with the new usage statistics data.
# (pay attention to the $_ ...)
#
sub update_quota
{

   my $array = shift;

   foreach( @{$array} )
   {
# alternative using for:
#   for ( my $line = 0 ; $line < @{$array} ; $line++ )
#      $_ = ${$array}[$line];
#            ${$array}[$line] = "$user:$limit:" . ( $usage + $dump->{$user}->{usage} ) . ":$disabled:$foo";

      next if /^[ \t]*$/;
      next if /^[ \t]*#/;

      my ( $user , $limit , $usage , $disabled , $foo ) = check_fields( $_ );

      foreach my $u ( @{$dump->{users}} )	# use $u, NOT $_ (or you'll mess up the two arrays)
      {
         if ( $u eq $user and defined $dump->{$user}->{usage} and $dump->{$user}->{usage} > 0 )
         {
            $_ = "$user:$limit:" . ( $usage + $dump->{$user}->{usage} ) . ":$disabled:$foo";
         }
      }
   }

} # update_quota #



###########################################################################
#_____________________________  CHECK_FIELDS _____________________________#
###########################################################################
#
# Sanity check for quotafile fields and values.
#
sub check_fields
{

   my $line = shift;

   die "*** BUG: undefined 'line'" unless defined $line;

   my ( $user , $limit , $usage , $disabled , $foo ) = split /:/ , $line , 5 ;

   die "*** 'user' undefined at line $.: [$line]\n"  unless defined $user && $user ;
   die "*** 'limit' undefined at line $.: [$line]\n" unless defined $limit;
   die "*** 'usage' undefined at line $.: [$line]\n" unless defined $usage;

   die "*** 'disabled' undefined at line $.: [$line]\n" unless defined $disabled;
   die "*** invalid 'disabled' value at line $.: [$line]\n" unless defined $disabled =~ /^[01]$/ ;

   die "*** invalid 'limit' value at line $.: [$user:<$limit>:$usage:$foo]\n" unless $limit =~ /^([0-9]+\.?[0-9]*|-1)$/ ;
   die "*** invalid 'usage' value at line $.: [$user:$limit:<$usage>:$foo]\n" unless $usage =~ /^[0-9]+\.?[0-9]*$/ ;

   return wantarray ? ( $user , $limit , $usage , $disabled , $foo ) : $line ;

} # check_fields #



##########################################################################
#_____________________________  NOTIFY_USER _____________________________#
##########################################################################
#
# Warn (by e-mail) admin and user about user's account (un)locking.
#
sub notify_user
{
   my $user   = $_[0] . '@' . $MAILHOST ;
   my $action = $_[1] ;
   my $act  = ( $action == DISABLE ) ? "dis" : "re-en" ;

   my $subject = "Account ${act}abled on $HOSTNAME";

   my $message = "Your account on $HOSTNAME has been ${act}abled";

   $message .= " because you have exceeded your cpu-time" if ( $action == DISABLE );

   print STDERR "\n\tuser [$user]: $subject\n\t$message\n\n" and return 0 if $INTERACTIVE;

warn "\aFIXME *** notify_user() disabled"; return 0;

   unless ( open( MAIL , "|mail '$user' -s '$subject' -c '$ADMIN'" ) )
   {
      warn "Cannot send e-mail, popen to mail failed: $!\n";
      return 1;
   }
   print MAIL <<EOF;

	$message

   Please contact $ADMINCONTACT.

EOF
   close ( MAIL );

   return 0;

} #  notify_user #



#######################################################################
#_____________________________  USER_PBS _____________________________#
#######################################################################
#
# Disable/re-enable user's pbs-submission privilege and warn him
# about it.
#
sub user_pbs
{
   my $user   = shift;
   my $action = shift ;
   my $act  = ( $action == DISABLE ) ? "dis" : "re-en" ;
   my $add  = ( $action == DISABLE ) ? "+" : "-" ;	# add or remove '-user'

   my $acl  = 'set queue dque acl_users';
   my $acl_user = "${acl}${add}='-${user}'";

warn "\aFIXME *** qmgr";
#   my $cmd = "qmgr";
   my $cmd = 'cat';

   print STDERR "${act}abling user account $user with command: [$cmd, acl $acl_user]\n";

   unless ( open( QMGR , "|$cmd" )  )
   {
      warn "Cannot ${act}able user $user, popen to $cmd failed: [$?] $!\n";
      return 1;
   }
   print QMGR $acl_user , "\n" ;
   close QMGR;

   return notify_user( $user , $action );

} # user_pbs #



###########################################################################
#_____________________________  USER_ACCOUNT _____________________________#
###########################################################################
#
# Disable/re-enable user's account and warn him about it.
#
sub user_account
{
   my $user   = shift;
   my $action = shift ;
   my $act  = ( $action == DISABLE ) ? "dis" : "re-en" ;
   my $when = ( $action == DISABLE ) ? $EXPIRENOW : $EXPIREGRC ;

warn "\aFIXME *** dummy chage";
   my $dummy = 'echo';

   my $cmd = "$dummy chage -E $when $user 2>&1";

   print STDERR "${act}abling user account $user with command: [$cmd]\n";

   my $chage = `$cmd`;
   if ($? >> 8)
   {
      warn "Cannot ${act}able user account!\n$chage\n";
      return 1;
   }

   return notify_user( $user , $action );

} # user_account #



##############################################################################
#_____________________________  SET_USER_STATUS _____________________________#
##############################################################################
#
# its return status might be misleading, is better to ignore it.
#
sub set_user_status
{
   my ( $line , $action , $reqline ) = @_;
   return FAILURE if not defined $action or ( $action != ENABLE and $action != DISABLE );

   my $new = ( $action == DISABLE ) ? DISABLE : ENABLE ;	# the new state is the request
   my $old = ( $action == DISABLE ) ? ENABLE : DISABLE ;	# the old state should be the opposite of the request
   warn "I'm going to change $old to $new...\n" if $DEBUG;

   my $status = undef;
   my ( $before , $after );
   {
      $_ = ${$line};
      chomp( $before = $_ );
      if ( $before eq $reqline && s/^([^:]+:)([^:]+:)([^:]+:)${old}(:.*)$/${1}${2}${3}${new}${4}/ )
      {
         $status = SUCCESS;
      }
      else
      {
         warn "cannot satisfy requested action ($old to $new)\n";
         warn "requested line doesn't match current line: [$reqline] vs [$before]\n";
         $status = FAILURE;
      }
      chomp( $after = $_ );
   }
   warn "changed [$before] to [$after]\n" if $status == SUCCESS;

   return $status;

} # set_user_status #



#######################  E N D   O F   F I L E  ########################

