#!/bin/bash

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Path: /etc/accounting/bin           #
# File: quacctset_user_limit.sh       #
# Date: 28 Aug 2004                   #
#-------------------------------------#
# Prev Modified: 28 Aug 2004          #
# Last Modified: 12 Sep 2004          #
#=====================================#

#	# ./quacctset_user_limit.sh baro 200
#	before: [baro:100:0:0:]
#	after:  [baro:200:0:0:]

[ "$(id -u)" != "0" ] && { echo >&2 "Sorry, you must be root." ; exit 1 ; }

QUOTAFILE=/etc/accounting/accounting.users
BKEXT=.__SETBCK__

function bell()
{
   [ -t 2 ] && echo -ne >&2 "\a"
}

function usage()
{
   test $# != 0  && { bell ; echo >&2 "*** usage error: $*" ; }
   echo >&2 "Usage: $0 USER LIMIT"
   exit $((!!$#))
}

function isnum()
{
   [ -z "${1//[0-9]/}" ]
}

test -z "$1" && usage "empty username"
test -z "$2" && usage "empty limit"
isnum $2     || usage "invalid limit '$2'"

user="$1"
limit="$2"

# BEFORE
{
   line="$(grep "^$user:" $QUOTAFILE)"
   test -z "$line" && {
      bell
      echo >&2 "*** user '$user' doesn't exist on quotafile '$QUOTAFILE'"
      exit 1
   }
   echo "before: [$line]"
}

# CHANGE
#sed    -i${BKEXT} -re "s/^($user:)[^:]+(:.*$)/\1${limit}\2/"       $QUOTAFILE
perl -p -i${BKEXT} -e  "s/^($user:)[^:]+(:.*$)/\${1}${limit}\${2}/" $QUOTAFILE

# AFTER
{
   line="$(grep "^$user:" $QUOTAFILE)"
   test -z "$line" && {
      bell
      echo >&2 "*** mmh, something's gone wrong, user's line is missing..."
      echo >&2 "*** please, look at the backup file: ${QUOTAFILE}${BKEXT}"
      exit 1
   }
   echo "after:  [$line]"
}

exit 0

#EOF
