/*
** qusage: PBS/Torque accounting parser
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   20 Aug 2004                            *
 * File:   pbs_accounting.c                       *
 *================================================*
 * Prev Modified: 20 Aug 2004                     *
 * Prev Modified: 23 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Prev Modified: 25 Aug 2004                     *
 * Prev Modified: 26 Aug 2004                     *
 * Prev Modified: 15 Sep 2004                     *
 * Prev Modified: 27 Oct 2007                     *
 * Last Modified: 10 Nov 2010                     *
 **************************************************/

/*
** pbs_accounting - program to summarize and report system usage based on
**                  PBS/Torque accounting input data.
**
** pbs_accounting revision:
**                - Moreno 'baro' Baricevic, DEMOCRITOS/CNR-IOM, baro@democritos.it
**                  10 Nov 2010:  introduced -Q/--queue option; fixed MAX_*NAME_SIZE
**
**                - Moreno 'baro' Baricevic, DEMOCRITOS/CNR-INFM, baro@democritos.it
**                  27 Oct 2007: fix for ncpus/nodect/nodes/ppn
**
**                - Moreno 'baro' Baricevic, DEMOCRITOS/CNR-INFM, baro@democritos.it
**                  24 Aug 2004: almost rewritten + bug fixes.
**
**                - Moreno 'baro' Baricevic, DEMOCRITOS/CNR-INFM, baro@democritos.it
**                  20 Aug 2004: First import based on pbs_acct-0.8 by Albino Aveleda,
**                               NACAD-COPPE/UFRJ, bino@nacad.ufrj.br
*/

#include "qusage.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <regex.h>

#define MAX_HOSTNAME_SIZE   1024	/* max host name length */
#define MAX_NAME_SIZE       32		/* max user/group name length */
#define MAX_QUEUE_NAME_SIZE 15		/* max queue name length */
//	./torque-2.5.3/src/include/pbs_ifl.h:#define PBS_MAXUSER  32 /* max user name length */
//	./torque-2.5.3/src/include/pbs_ifl.h:#define PBS_MAXGRPN  16 /* max group name length */
//	./torque-2.5.3/src/include/pbs_ifl.h:#define PBS_MAXQUEUENAME 15 /* max queue name length */
//	./torque-2.5.3/src/include/pbs_ifl.h:#define PBS_MAXHOSTNAME  1024 /* max host name length */

/* set this to 1 to report usage in cputime rather than walltime */
#define USE_CPUTIME 0

typedef struct {
   int users;
   int groups;
   int num_jobs;
   double cpu_time;
   double all_cpu_time;
} total_s;

typedef struct _group_s group_s;
typedef struct _user_s  user_s;

struct _group_s {
   char groupname[MAX_NAME_SIZE];
   int num_jobs;
   double cpu_time;
   double all_cpu_time;
   group_s * next;
};

struct _user_s {
   char username[MAX_NAME_SIZE];
   int num_jobs;
   double cpu_time;
   double all_cpu_time;
   group_s * pg;
   user_s * next;
};


typedef struct {
   int    nodes;
   double cpu_used;
   char   uid[MAX_NAME_SIZE];
   char   gid[MAX_NAME_SIZE];
} acct_s;



#define new( object )	(object *)xcmalloc( sizeof( object ) )



/*********************************************************************
 *_____________________________  SORT_* _____________________________*
**********************************************************************/
// Sorting routine grabbed and adapted from "The C Book":
//	http://publications.gbdirect.co.uk/c_book/chapter6/structures.html
/*
** Algorithm is this:
** Repeatedly scan list.
** If two list items are out of order,
** link them in the other way round.
** Stop if a full pass is made and no
** exchanges are required.
** The whole business is confused by
** working one element behind the
** first one of interest.
** This is because of the simple mechanics of
** linking and unlinking elements.
*/
#define SORT_INT( func , obj , what )					\
static obj * sort_ ## obj ## _  ## func ( obj * list , int order )	\
{									\
									\
   int exchange;							\
   obj * nextp , * thisp , dummy;					\
									\
   dummy.next = list;							\
									\
   do {									\
      exchange = 0;							\
      thisp = &dummy;							\
      while( ( nextp = thisp->next ) && nextp->next )			\
      {									\
         int x;								\
         if ( order == REVERSE )					\
           x = (int)( nextp->what < nextp->next->what );		\
         else								\
           x = (int)( nextp->what > nextp->next->what );		\
         if ( x )							\
         {								\
            /* exchange */						\
            exchange = 1;						\
            thisp->next = nextp->next;					\
            nextp->next = thisp->next->next;				\
            thisp->next->next = nextp;					\
         }								\
         thisp = thisp->next;						\
      }									\
   } while ( exchange );						\
									\
   return dummy.next;							\
} /* sort_user* */

SORT_INT( jobs  , user_s , num_jobs );
SORT_INT( cput  , user_s , cpu_time );
SORT_INT( acput , user_s , all_cpu_time );

SORT_INT( jobs  , group_s , num_jobs );
SORT_INT( cput  , group_s , cpu_time );
SORT_INT( acput , group_s , all_cpu_time );


#define SORT_STR( func , obj , what )					\
static obj * sort_ ## obj ## _  ## func ( obj * list , int order )	\
{									\
									\
   int exchange;							\
   obj * nextp , * thisp , dummy;					\
									\
   dummy.next = list;							\
									\
   do {									\
      exchange = 0;							\
      thisp = &dummy;							\
      while( ( nextp = thisp->next ) && nextp->next )			\
      {									\
         int x;								\
         if ( order == REVERSE )					\
           x = ( strncmp( nextp->what ,	nextp->next->what ,		\
                          MAX_NAME_SIZE ) < 0 );			\
         else								\
           x = ( strncmp( nextp->what , nextp->next->what ,		\
                          MAX_NAME_SIZE ) > 0 );			\
         if ( x )							\
         {								\
            /* exchange */						\
            exchange = 1;						\
            thisp->next = nextp->next;					\
            nextp->next = thisp->next->next;				\
            thisp->next->next = nextp;					\
         }								\
         thisp = thisp->next;						\
      }									\
   } while ( exchange );						\
									\
   return dummy.next;							\
} /* sort_user* */

SORT_STR( user  , user_s , username );
SORT_STR( group , user_s , pg->groupname );

SORT_STR( group  , group_s , groupname );



/***************************************************************************
 *_____________________________  CLEAN_GROUPS _____________________________*
 ***************************************************************************/
static void clean_groups( group_s * ptr )
{
   group_s * nextp;

   while ( ptr && ptr->next )
   {
      nextp = ptr->next;
      xfree( ptr );
      ptr = nextp;
   }
} /* clean_groups */



/**************************************************************************
 *_____________________________  CLEAN_USERS _____________________________*
 **************************************************************************/
static void clean_users( user_s * ptr )
{
   user_s * nextp;

   while ( ptr && ptr->next )
   {
      nextp = ptr->next;
      xfree( ptr );
      ptr = nextp;
   }
} /* clean_users */


/**************************************************************************
 *_____________________________  INSERT_USER _____________________________*
 **************************************************************************/
static user_s * insert_user ( user_s ** ptr , const acct_s * accts )
{

   user_s * walk = (*ptr);

   int test = strncmp( accts->uid , walk->username , MAX_NAME_SIZE );
   if ( test )
   {
      if ( test < 0 )
      {
         (*ptr) = new( user_s );
         (*ptr)->next = walk;
         walk = (*ptr);
         strncpy( walk->username , accts->uid , MAX_NAME_SIZE );
      }
      else
      {
         while (
                  ( walk->next ) &&
                  ( strncmp( accts->uid , walk->next->username , MAX_NAME_SIZE ) >= 0 )
               )
           walk = walk->next;

         if ( strncmp( accts->uid , walk->username , MAX_NAME_SIZE ) > 0 )
         {
            if ( ! walk->next )
            {
               walk->next = new( user_s );
            }
            else
            {
               user_s * ins = new( user_s );
               ins->next = walk->next;
               walk->next = ins;
            }
            walk = walk->next;
            strncpy( walk->username , accts->uid , MAX_NAME_SIZE );
         }
      }
   }
   return walk;

} /* insert_user */


/***************************************************************************
 *_____________________________  INSERT_GROUP _____________________________*
 ***************************************************************************/
static group_s * insert_group ( group_s ** ptr , const acct_s * accts )
{

   group_s * walk = (*ptr);

   int test = strncmp( accts->gid , walk->groupname , MAX_NAME_SIZE );
   if ( test )
   {
      if ( test < 0 )
      {
         (*ptr) = new( group_s );
         (*ptr)->next = walk;
         walk = (*ptr);
         strncpy( walk->groupname , accts->gid , MAX_NAME_SIZE );
      }
      else
      {
         while (
                  ( walk->next ) &&
                  ( strncmp( accts->gid , walk->next->groupname , MAX_NAME_SIZE ) >= 0 )
               )
           walk = walk->next;

         if ( strncmp( accts->gid , walk->groupname , MAX_NAME_SIZE ) > 0 )
         {
            if ( ! walk->next )
            {
               walk->next = new( group_s );
            }
            else
            {
               group_s * ins = new( group_s );
               ins->next = walk->next;
               walk->next = ins;
            }
            walk = walk->next;
            strncpy( walk->groupname , accts->gid , MAX_NAME_SIZE );
         }
      }
   }
   return walk;

} /* insert_group */



static group_s * pgroups = NULL;
static user_s  * pusers = NULL;
static total_s total = { 0 , 0 , 0 , 0.0 , 0.0 };


/************************************************************************
 *_____________________________  SET_ACCTS _____________________________*
 ************************************************************************/
/*
** accounting structure assignment
*/
static void set_accts ( const acct_s * accts )
{

   user_s  * pu;
   group_s * pg;
   double cputime;

   if ( opt.use_cput )	/* cputime */
	cputime = accts->cpu_used;
   else			/* walltime */
	cputime = accts->nodes * accts->cpu_used;

/*
** lookup group
*/
   if ( ! pgroups )
   {
      pgroups = new( group_s );
      pg = pgroups;
      strncpy( pg->groupname , accts->gid , MAX_NAME_SIZE );
   }
   else
     pg = insert_group( &pgroups , accts );

   pg->num_jobs++;
   pg->cpu_time += accts->cpu_used;
   pg->all_cpu_time += cputime;

/*
** lookup user
*/
   if ( ! pusers )
   {
      pusers = new( user_s );
      pu = pusers;
      strncpy( pu->username , accts->uid , MAX_NAME_SIZE );
   }
   else
     pu = insert_user( &pusers , accts );

   pu->num_jobs++;
   pu->cpu_time += accts->cpu_used;
   pu->all_cpu_time += cputime;
   pu->pg = pg;

   total.num_jobs++;
   total.cpu_time += accts->cpu_used;
   total.all_cpu_time += cputime;

} /* set_accts */



#define SKIP( msg , args... )	do{					\
					VRBprint( msg , ## args );	\
					xfree( tmpline );		\
					return FALSE;			\
				}while(0)

#define LOOK(x)		\
	DBGprint( "%s = [%s]\n" , #x , x );


#ifdef PROFILING

#define COPY( tmpline , pbsline )		\
	memcpy( tmpline , pbsline , len );

#define COPY( tmpline , pbsline )		\
	memcpy( tmpline , pbsline , len );	\
	*(tmpline+len) = 0;

#define COPY( tmpline , pbsline )		\
	memset( tmpline , 0 , len );		\
	memcpy( tmpline , pbsline , len );

#define COPY( tmpline , pbsline )		\
	strncpy( tmpline , pbsline , len );

#define COPY( tmpline , pbsline )		\
	snprintf( tmpline , len , pbsline );

#endif /* PROFILING */

#define COPY( tmpline , pbsline )		\
	memcpy( tmpline , pbsline , len );



/************************************************************************
 *_____________________________  GET_NCPUS _____________________________*
 ************************************************************************/
/*
** calculate total number of cpus
*/
typedef struct {
	const char * match_str;
	const char * scanf_str;
	char * buf;
	int val;
} res_t ;
static int get_substr ( const char * string , res_t * resource )
{
	char * start = strstr( string , resource->match_str );
	if ( start )
	{
		char * end = strchr( start , ' ' );
		if ( end > start )
		{
			resource->buf = xcmalloc( end - start + 1 );
			memcpy( resource->buf , start , end-start );
			return 1;
		}
	}
	return 0;
} /* get_substr */
static int get_ncpus ( const char * acctline , size_t len )
{
	res_t r_ncpus  = { "Resource_List.ncpus="	, "Resource_List.ncpus=%d"	, NULL , -1 };
	res_t r_nodes  = { "Resource_List.nodes="	, "Resource_List.nodes=%d"	, NULL , -1 };
	res_t l_nodes  = { "Resource_List.nodes="	, "Resource_List.nodes=%[^ ]s"	, NULL , -1 };
	res_t r_nodect = { "Resource_List.nodect="	, "Resource_List.nodect=%d"	, NULL , -1 };
	res_t r_ppn    = { "ppn="			, "%d"				, NULL , 0  };

	int tot = 1;

	char * tmpline = (char *)xcmalloc( len );
	COPY( tmpline , acctline );

#define GETS(r)	( get_substr( tmpline , &r ) )
#define SCAN(r)	( sscanf( r.buf , r.scanf_str , &r.val ) == 1 && r.val != -1 )

	VRBprint( ">>> got:[" );

	if ( GETS(r_nodes) )
	{
		if ( SCAN(r_nodes)  )
		{
			VRBprint( " nodes" );
			tot = r_nodes.val;
			if ( GETS(r_ncpus) )
			{
				VRBprint( " ncpus" );
				if ( SCAN(r_ncpus) )
				{
					tot *= r_ncpus.val;
				}
				xfree( r_ncpus.buf );
			}
		}

		if ( GETS(l_nodes) )
		{
			char * t = strstr( l_nodes.buf , r_ppn.match_str );
			if ( t )
			{
				int offt = strlen( r_ppn.match_str );
				while ( t )
				{
					int ppn = -1;
					VRBprint( " ppn" );
//					VRBprint( "\n--- %p: %s\n" , t , t );
					t += offt;
					if ( sscanf( t , r_ppn.scanf_str , &ppn ) == 1 && ppn != -1 )
						r_ppn.val += ppn;
					t = strstr( t , r_ppn.match_str );
				}
				tot = r_ppn.val;
				if ( r_nodes.val > 0 )
					tot *= r_nodes.val;
			}
			xfree( l_nodes.buf );
		}
		xfree( r_nodes.buf );
	}
	else if ( GETS(r_nodect) )
	{
		VRBprint( " nodect" );
		if ( SCAN(r_nodect) )
		{
			tot *= r_nodect.val;
		}
		xfree( r_nodect.buf );
	}

	if ( opt.verbose )
	{
		char * p = NULL , * t = NULL , * saved = NULL ;

		// terminate the input string before useless data
		if ( ( p = strstr( tmpline , " session=" ) ) )
			*p = 0;
		if ( ( p = strstr( tmpline , " Resource_List.walltime" ) ) )
			*p = 0;
		if ( ( p = strstr( tmpline , " Resource_List.pcput" ) ) )
			*p = 0;
		if ( ( p = strstr( tmpline , " Resource_List.vmem" ) ) )
			*p = 0;

		// move start of string to 1st useful data
		p = strstr( tmpline , "Resource_List." );
		if ( ! p )
			p = tmpline;
		// skip useless data, if any
#define USELESS(what)	do{											\
				saved = p;									\
				if ( ( t = strstr( p , what ) ) && ( p = strstr( t , " R" ) ) == NULL )		\
						p = saved;							\
			}while(0)
		USELESS("Resource_List.mem=");
		USELESS("Resource_List.cput=");
		VRBprint( " ] [%s]\n" , p );
	}

	DBGprint( "==============[scanf] (ncpus:%d,nodes:%d,nodect:%d,ppn:%d) = %d\n" , r_ncpus.val , r_nodes.val , r_nodect.val , r_ppn.val , tot );

	xfree(tmpline);

	return tot;

} /* parse_acctline*/



/*****************************************************************************
 *_____________________________  PARSE_ACCTLINE _____________________________*
 *****************************************************************************/
/*
** pbs accounting line parsing
*/
flag_t parse_acctline( int entry , const char * pbsline , size_t len )
{

   acct_s accts = { 0 , 0.0 , {0} , {0} } ;

   {
      int hour , min , sec;
      char * p , * ptr , * tmpline;

      tmpline = (char *)xcmalloc( len );

#ifdef PROFILING
{
      long long int x = 0LL , y = 0LL;
      DBGprint( "p eos = ...|%02X|%02X|%02X]; len %d - strlen %d\n" , *(pbsline+len-3)  , *(pbsline+len-2)  , *(pbsline+len-1) , len , strlen(pbsline) );
      DBGprint( "t eos = ...|%02X|%02X|%02X]; len %d - strlen %d\n" , *(tmpline+len-3)  , *(tmpline+len-2)  , *(tmpline+len-1) , len , strlen(tmpline) );
      x = rdtsc();
      COPY( tmpline , pbsline );
      y = rdtsc();
      DBGprint( ">>>>>>>>>>>>>>>> %lld\n" , y - x );
      DBGprint( "t eos = ...|%02X|%02X|%02X]; len %d - strlen %d\n" , *(tmpline+len-3)  , *(tmpline+len-2)  , *(tmpline+len-1) , len , strlen(tmpline) );
}
#endif

      /* search for user name */
      COPY( tmpline , pbsline );
      ptr = strstr( tmpline , ";user" );		//	[;user=luser group=lusers ...]
      if ( ! ptr )
      {
         SKIP( "Warning: No user (id) data on entry %d, skipping...\n" , entry );
      }
      p = strtok( ptr  , "=" );				//	[;user]=...
      p = strtok( NULL , "=" );				//	[luser group]=...
      if ( sscanf( p , "%s " , accts.uid ) != 1 )	//	[luser]
      {
         VRBprint( "Warning: undefined user on entry %d, assuming 'unknown'...\n" , entry );
         strncpy( accts.uid , "unknown" , MAX_NAME_SIZE );
      }
      p = strtok( NULL , "=" );				//	[lusers jobname]=...
      if ( sscanf( p , "%s " , accts.gid ) != 1 )	//	[lusers]
      {
         VRBprint( "Warning: undefined group on entry %d, assuming 'unknown'...\n" , entry );
         strncpy( accts.gid , "unknown" , MAX_NAME_SIZE );
      }

      /* continue only if uid match the request user */
      if ( opt.user && strncmp( opt.user , accts.uid , MAX_NAME_SIZE ) )
      {
         SKIP( "Warning: user %s doesn't match %s on entry %d, skipping...\n"
               , accts.uid , opt.user , entry );
      }

      /* continue only if gid match the request group */
      if ( opt.group && strncmp( opt.group , accts.gid , MAX_NAME_SIZE ) )
      {
         SKIP( "Warning: group %s doesn't match %s on entry %d, skipping...\n"
               , accts.gid , opt.group , entry );
      }

{     /* continue only in case the queue matches the requested one */

      char queue[MAX_QUEUE_NAME_SIZE];

      COPY( tmpline , pbsline );
      ptr = strstr( tmpline , " queue=" );	//	[ queue]=...
      if ( ! ptr )
      {
         NQprint( "Warning: this wasn't supposed to happen, 'queue=' not defined for entry %d. I think I'll miserably die now.\n" , entry );
         abort();
      }
      p = strtok( ptr  , "=" );			//	[queue]=QUEUE ...
      p = strtok( NULL , "=" );			//	queue=[QUEUE ...]
      if ( sscanf( p , "%s " , queue ) != 1 )	//	[QUEUE]
      {
         VRBprint( "Warning: this wasn't supposed to happen, undefined queue on entry %d. I think I'll miserably die now.\n" , entry );
         abort();
      }
      if ( opt.queue )
      {
         int match = strncmp( opt.queue , queue , MAX_QUEUE_NAME_SIZE );
         if (    ( ! opt.queue_invert_match && match != 0 )		// skup if straight match, but no match found
              || (   opt.queue_invert_match && match == 0 ) )		// skip if inverse match, but match found
         {
            SKIP( "Warning: queue %s doesn't match %s on entry %d, skipping...\n"
                  , queue , opt.queue , entry );
         }
      }
}

      accts.nodes = get_ncpus( pbsline , len );

      COPY( tmpline , pbsline );
      if ( opt.use_cput )
      {
         /* search for cpu or wall time */
         ptr = strstr( tmpline , "resources_used.cput" );	//	[resources_used.cput=00:51:29...]
         if ( ! ptr )
         {
            SKIP( "Warning: No used.cput data on entry %d, skipping...\n" , entry );
         }
      }
      else
      {
         /* use walltime instead */
         ptr = strstr( tmpline , "resources_used.walltime" );	//	[resources_used.walltime=00:49:39](EOL)
         if ( ! ptr )
         {
            SKIP( "Warning: No used.walltime data on entry %d, skipping...\n" , entry );
         }
      }
      p = strtok( ptr  , "=" );				//	[resources_used.walltime]=... | [resources_used.cput]=...
      p = strtok( NULL , "=" );				//	[00:49:39]...
      if ( sscanf( p , "%d:%d:%d " , &hour , &min , &sec ) != 3 )
      {
         hour = ( hour < 0 ) ? 0 : hour;
         min  = ( min  < 0 ) ? 0 : min ;
         sec  = ( sec  < 0 ) ? 0 : sec ;
         SKIP( "Warning: No HMS on entry %d [%s]\n" , entry , p );
      }
      accts.cpu_used = (double)( (double)sec / 60 ) + min + ( hour * 60 );

      xfree( tmpline );
   }

   VRBprint("--- user=%s, group=%s, cpus=%d, cputime=%-.4f;\n",accts.uid,accts.gid,accts.nodes,accts.cpu_used);

   set_accts( &accts );

   return TRUE;

} /* parse_acctline*/



/***************************************************************************
 *_____________________________  PRINT_REPORT _____________________________*
 ***************************************************************************/
void print_report( void )
{

   char myname[MAX_HOSTNAME_SIZE + 1] = {0};

/*
** get the hostname
*/
   if ( gethostname( myname , sizeof( myname ) ) == -1 )
     strncpy( myname , "unknown" , MAX_HOSTNAME_SIZE );

   if ( ! opt.quiet )
     printf(
             "\n*** Portable Batch System accounting statistics ***\n"
             "Server Name: %s\n"
             "Reporting %s\n"
             , myname , opt.use_cput ? "cputime" : "walltime"
           );

   if ( ! opt.quiet && opt.queue )
     printf(
             "Filtering by queue: %s%s\n"
             , (opt.queue_invert_match)?"(not)":"" , opt.queue
           );

/*
** print report user
*/
   if ( pusers && ISSHOW( USERS ) )
   {
      user_s  * pu;

      if ( opt.sort )
      {
         switch ( opt.sort )
         {
            case 'J': pusers = sort_user_s_jobs(  pusers , opt.order ); break;
            case 'U': pusers = sort_user_s_user(  pusers , opt.order ); break;
            case 'C': pusers = sort_user_s_cput(  pusers , opt.order ); break;
            case 'A': pusers = sort_user_s_acput( pusers , opt.order ); break;
            case 'G': pusers = sort_user_s_group( pusers , opt.order ); break;
         }
      }

      if ( ! opt.quiet )
        printf(
                "\n*** PBS Per-User Usage Report ***\n"
                "User         Group         #Jobs        Job-Hours        CPU-Hours\n"
                "__________________________________________________________________\n"
              );

      pu = pusers;
      while ( pu )
      {
         if ( opt.spacer >= 0 )
         {
            char sp = (char)opt.spacer;
            printf( "%s%c%s%c%d%c%.4f%c%.4f\n"
                    , pu->username , sp , pu->pg->groupname , sp , pu->num_jobs
                    , sp , ( pu->cpu_time / 60 ) , sp , ( pu->all_cpu_time / 60 )
                  );
         }
         else
         {
            printf( "%-12s %-12s %6d %16.4f %16.4f\n"
                    , pu->username , pu->pg->groupname , pu->num_jobs
                    , ( pu->cpu_time / 60 ) , ( pu->all_cpu_time / 60 )
                  );
         }
         pu = pu->next;
      }
   }

/*
** print report group
*/
   if ( pgroups && ISSHOW( GROUPS ) )
   {
      group_s * pg;

      if ( opt.sort )
      {
         switch ( opt.sort )
         {
            case 'J': pgroups = sort_group_s_jobs(  pgroups , opt.order ); break;
            case 'U': break;	// nothing to do here, no username
            case 'C': pgroups = sort_group_s_cput(  pgroups , opt.order ); break;
            case 'A': pgroups = sort_group_s_acput( pgroups , opt.order ); break;
            case 'G': pgroups = sort_group_s_group( pgroups , opt.order ); break;
         }
      }

      if ( ! opt.quiet )
        printf(
                "\n*** PBS Per-Group Usage Report ***\n"
                "Group         #Jobs        Job-Hours        CPU-Hours\n"
                "_____________________________________________________\n"
              );

      pg = pgroups;
      while ( pg )
      {
         if ( opt.spacer >= 0 )
         {
            char sp = (char)opt.spacer;
            printf( "%s%c%d%c%.4f%c%.4f\n"
                    , pg->groupname , sp , pg->num_jobs , sp
                    , ( pg->cpu_time / 60 ) , sp , ( pg->all_cpu_time / 60 )
                  );
         }
         else
         {
            printf( "%-12s %6d %16.4f %16.4f\n"
                    , pg->groupname , pg->num_jobs
                    , ( pg->cpu_time / 60 ) , ( pg->all_cpu_time / 60 )
                  );
         }
         pg = pg->next;
      }
   }

   if ( ! pgroups && ! pusers )
   {
      NQprint( "\n*** NO GROUPS AND NO USERS !!!\n\n" );
   }
   else if ( ISSHOW( TOTAL ) )
   {
      user_s  * u ;
      group_s * g ;
      for( u = pusers  ; u ; u = u->next , ++total.users  );
      for( g = pgroups ; g ; g = g->next , ++total.groups );

      if ( ! opt.quiet )
        printf(
                "\n*** PBS Total Usage Report ***\n"
                "Users  Groups  #Jobs        Job-Hours        CPU-Hours\n"
                "______________________________________________________\n"
              );

      printf( "%5d %7d %6d %16.4f %16.4f\n"
              , total.users , total.groups , total.num_jobs
              , ( total.cpu_time / 60 ) , ( total.all_cpu_time / 60 )
            );
   }

   if ( pgroups ) clean_groups( pgroups );
   if ( pusers )  clean_users( pusers );

} /* print_report */




/***********************  E N D   O F   F I L E  ************************/

