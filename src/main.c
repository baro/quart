/*
** qusage: main file
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   22 Aug 2004                            *
 * File:   main.c                                 *
 *================================================*
 * Prev Modified: 22 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Prev Modified: 25 Aug 2004                     *
 * Prev Modified: 26 Aug 2004                     *
 * Last Modified: 29 Aug 2004                     *
  **************************************************/

#include "qusage.h"


int
main ( argc , argv )
   int argc ;
   char * argv[];
{

   char ** file ;
   int files = 0;
   int entries = 0;

   int usage_error = options( argc , argv );

   if ( usage_error )
     exit( usage_error );

   /* force reading from stdin */

   if ( opt.use_stdin )
   {
      entries = read_files( NULL , NULL );
      if ( entries > 0 ) 
        print_report();
      return( ! entries );
   }

   /* sanity check on input files */

   file = opt.first_file;
   while ( file <= opt.last_file )
     if ( ! file_ok( *file ) )
       exit( 1 );
     else
       ++file , ++files;

   /* real job */

   VRBprint( "%d file%s to parse\n" , files , (files>1)?"s":"" );

   entries = read_files( opt.first_file , opt.last_file );

   VRBprint( "%d entr%s found\n" , entries , (entries==1)?"y":"ies" );

   if ( opt.verbose )
   {
      fprintf( stderr , "%d file%s parsed:\n" , files , (files>1)?"s":"" );
      file = opt.first_file;
      while ( file <= opt.last_file )
        fprintf( stderr , "\t%s\n" , *(file++) );
      fprintf( stderr , "\n" );
   }

   if ( entries > 0 ) 
     print_report();

   return( ! entries );

} /* main */



/***********************  E N D   O F   F I L E  ************************/

