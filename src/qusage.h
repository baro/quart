/*
** qusage: header file
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   22 Aug 2004                            *
 * File:   qusage.h                               *
 *================================================*
 * Prev Modified: 22 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Prev Modified: 26 Aug 2004                     *
 * Prev Modified: 29 Aug 2004                     *
 * Prev Modified: 27 Oct 2007                     *
 * Last Modified: 10 Nov 2010                     *
 **************************************************/

#ifndef _QUSAGE_H_
#define _QUSAGE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define PROGRAM_NAME	"qusage"
#define VERSION		"1.3"

#define TRUE	1
#define FALSE	(!TRUE)
typedef int flag_t;


typedef struct {

  int debug ;
  int quiet ;
  int verbose ;

  char ** first_file ;
  char ** last_file ;

  char * user ;
  char * group ;

  char * queue ;
  int queue_invert_match ;

  int show ;

  int use_cput ;	// use cputime instead of walltime

  char sort ;
  int order ;

  short spacer ;

  int use_stdin ;

} opt_t;

extern opt_t opt;


/*
** sorting order flags
*/
#define REVERSE		1
#define	DIRECT		(!REVERSE)

/*
** one flag for both, use cputime or walltime
*/
#define USE_CPUT	1
#define USE_WALL	(!USE_CPUT)

/*
** show everything, only users or only groups
*/
// flags
#define SHOW_NOP	0x00
#define SHOW_USERS	0x10
#define SHOW_GROUPS	0x20
#define SHOW_UGMASK	0x30
#define SHOW_TOTAL	0x01
#define SHOW_STATS	0x02
#define SHOW_TSMASK	0x03
#define SHOW_ALL	0x33
#define SHOW_DFLT	( SHOW_USERS | SHOW_GROUPS )
// assignments
#define SHOW( what )		opt.show |= SHOW_ ## what
#define HIDE( what )		opt.show &= ~( SHOW_ ## what )
//#define SETSHOW( what )		opt.show = ( ( opt.show & ~SHOW_ALL ) | SHOW_ ## what )
// boolean
#define ISSHOW( what )		( opt.show & SHOW_ ## what )
#define SHOW_ONLY( what )	( ! ( ( opt.show & SHOW_UGMASK ) ^ SHOW_ ## what ) )
#define SHOW_EQ( what )		( ! ( ( opt.show & SHOW_ ## what ) ^ SHOW_ ## what ) )
				// == ( ( opt.show & SHOW_ALL ) == SHOW_ALL )


#define VRBprint( fmt , args... )			\
	do{						\
	   if ( opt.verbose ) 				\
	   {						\
	      fprintf( stderr , fmt , ## args );	\
	   }						\
	}while(0)

#define NQprint( fmt , args... )			\
	do{						\
	   if ( ! opt.quiet ) 				\
	   {						\
	      fprintf( stderr , fmt , ## args );	\
	   }						\
	}while(0)

#define DEBUG
#ifdef DEBUG
#define DBGprint( fmt , args... )				\
	do{							\
	   if ( opt.debug ) 					\
	   {							\
	      fprintf( stderr ,					\
	               "[%-16.16s:%3d][%-12.12s] " fmt ,	\
	               __FILE__ , __LINE__ , __FUNCTION__	\
	               , ## args );				\
	   }							\
	}while(0)
#else
# define DBGprint( args... ) do{}while(0)
#endif


#define MARK	do{ fprintf( stderr , "%d: MARK\n" , __LINE__ ); }while(0)

#define BELL	do{ if ( isatty( STDERR_FILENO ) ) putc( '\a' , stderr ); }while(0)

/*
** Functions declaration
*/

extern int main ( int argc , char * argv[] );

extern int options ( int opt_argc , char ** opt_argv );

extern void die ( const char * errmsg , ... );
#ifdef PROFILING
extern __inline__ unsigned long long int rdtsc( void );
#endif
extern void * xcmalloc ( size_t size );
extern void xfree ( void * ptr );
extern void fatal ( const char * msg );

extern flag_t file_ok( char * file );
extern int read_files( char ** first , char ** last );

extern flag_t parse_acctline( int entry , const char * pbsline , size_t len );
extern void print_report( void );


#endif /* _QUSAGE_H_ */




/***********************  E N D   O F   F I L E  ************************/

