/*
** qusage: additional routines
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   22 Aug 2004                            *
 * File:   utils.c                                *
 *================================================*
 * Prev Modified: 22 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Last Modified: 26 Aug 2004                     *
  **************************************************/

#include "qusage.h"


#include <stdarg.h>
/******************************************************************
 *_____________________________  DIE _____________________________*
 ******************************************************************/
/*
** mmh, something's gone wrong
*/
void die ( const char * errmsg , ... )
{

   if ( opt.quiet )
     exit( 1 );

   if ( errno )
     fprintf( stderr , "*** errno: [%d] %m\n" , errno );

   if ( errmsg && *errmsg )
   {
      va_list args;
      va_start( args , errmsg );
      vfprintf( stderr , errmsg , args );
      va_end( args );
      putc( '\n' , stderr );
   }
   exit( 1 );

} /* die */



#ifdef PROFILING
/********************************************************************
 *_____________________________  RDTSC _____________________________*
 ********************************************************************/
__inline__ unsigned long long int rdtsc()
{
   unsigned long long int x;
   __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
   return x;
} /* rdtsc */
#endif



/***********************************************************************
 *_____________________________  XCMALLOC _____________________________*
 ***********************************************************************/
/* (om)procps/proc/ominfo.c */
/*
** allocates zeroed memory buffer.
** Dies only on system failure (ENOMEM), not bugs(?) like size<=0.
*/
void * xcmalloc ( const size_t size )
{

   errno = 0;

   if ( size > 0 )
   {
      register void * ptr = malloc( size );
      if ( ptr == NULL )
      {
         fprintf( stderr , "%s: allocation error, size = %zd\n"
                         , __FUNCTION__ , size );
         exit( 1 );
      }
      DBGprint( "%p: %4zd bytes allocated\n" , ptr , size );
      return memset( ptr , 0 , size );
   }

   errno = EINVAL;

   return NULL;

} /* xcmalloc */



/********************************************************************
 *_____________________________  XFREE _____________________________*
 ********************************************************************/
void xfree ( void * ptr )
{
   if ( ptr )
   {
      DBGprint( "%p: freeing pointer\n" , ptr );
      free( ptr );
      ptr = NULL;
   }
} /* xfree */



/********************************************************************
 *_____________________________  FATAL _____________________________*
 ********************************************************************/
void fatal ( const char * msg )
{
   fprintf( stderr , "%s: *** fatal: %s" , PROGRAM_NAME , (strlen(msg)) ? msg : "Oops!" );
   abort();
} /* fatal */



/***********************  E N D   O F   F I L E  ************************/

