/*
** qusage: sanity checks on input file(s) and input lines selection
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   22 Aug 2004                            *
 * File:   file.c                                 *
 *================================================*
 * Prev Modified: 22 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Last Modified: 26 Aug 2004                     *
 **************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "qusage.h"

#define isfile(x)	( (S_ISREG(x)) || (0==(x&S_IFMT)) )
#define isfifo(x)	(S_ISFIFO(x))

/**********************************************************************
 *_____________________________  FILE_OK _____________________________*
 **********************************************************************/
/*
** sanity checks for input file (regular file or fifo and readable)
*/
flag_t file_ok( char * file )
{
   struct stat st;

   if ( stat( file , &st ) < 0 )
   {
      NQprint( "*** file %s does not exist: [%d] %m\n" , file , errno );
      return FALSE;
   }

   /* regular file or fifo */
   {
      if ( ! isfile( st.st_mode ) && ! isfifo( st.st_mode ) )
      {
         NQprint( "*** file %s is not a regular file or fifo\n" , file );
         return FALSE;
      }
   }

   /* readable */
   {
      static int euid = -1;
      int mode = R_OK;

      if ( euid == -1 )
        euid = geteuid();

      if ( euid == 0 )	/* Root can read or write any file. */
        return TRUE;

      if ( st.st_uid == euid )
        /* owner */ mode <<= 6;
      else if ( ( st.st_gid == getgid() ) || ( st.st_gid == getegid() ) )
        /* group */ mode <<= 3;

      if ( st.st_mode & mode )
        return TRUE;
    }

    NQprint( "*** file %s is not readable\n" , file );
    return FALSE;

} /* file_ok */



/*****************************************************************************
 *_____________________________  GET_ENDED_JOBS _____________________________*
 *****************************************************************************/
/*
** select and parse ended jobs from stream (e.g. "03/29/2004 16:44:52;E;...")
*/
static int get_ended_jobs( const char * file , FILE * stream )
{
   int entry = 0;
   char * line = NULL;
   int linenum = 0;
   size_t len = 0;
   ssize_t size;

   while ( ( size = getline( &line , &len , stream ) ) != -1 )
   {
      ++linenum;
      if ( size < 21 )
        continue;
      if ( *(line+size-1) == '\n' )
        *(line+size-1) = '\0';

//      if ( *(line+20) == 'E' )
      if ( ! ( strncmp( (line+19) , ";E;" , 3 ) ) )
      {
         ++entry;

         VRBprint( "entry #%d, file %s, line %d: [%22.22s]\n"
                   , entry , file , linenum , line );

         // note that size doesn't include trailing '\0'
         (void)parse_acctline( entry , line , (size_t)( size + 1 )  );
      }
   }

   if ( line )
     free( line );

   return entry;

} /* get_ended_jobs */



/*************************************************************************
 *_____________________________  READ_FILES _____________________________*
 *************************************************************************/
/*
** read file(s) and select ended jobs
*/
int read_files( char ** first , char ** last )
{
   int entry = 0;

   if ( opt.use_stdin )
   {
      VRBprint( "reading from STDIN\n" );
      return get_ended_jobs( "STDIN" , stdin );
   }

   while( first <= last )
   {
      FILE * stream;
      stream = fopen( *first , "r" );
      if ( ! stream )
      {
         NQprint( "*** unable to open file %s: [%d] %m\n" , *first , errno );
         exit( 1 );
      }

      VRBprint( "parsing file '%s'\n" , *first );
      entry += get_ended_jobs( *first , stream );
      fclose( stream );
      ++first;
   }
   return entry;

} /* read_files */



/***********************  E N D   O F   F I L E  ************************/

