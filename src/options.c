/*
** qusage: command line options parser
**
** Copyright (C) 2004-2011  Moreno 'baro' Baricevic
**                                      <baro AT democritos DOT it>
**
** This  file may be used subject to the terms and conditions of the
** GNU Library General Public License as published by the Free Soft-
** ware Foundation; either version 2 of the License, or (at your op-
** tion) any later version.
**
** This file is distributed in the hope that it will be useful,  but
** WITHOUT  ANY  WARRANTY; without even the implied warranty of MER-
** CHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Li-
** brary General Public License for more details.
*/

/**************************************************
 * Author: Moreno 'baro' Baricevic                *
 * Date:   22 Aug 2004                            *
 * File:   options.c                              *
 *================================================*
 * Prev Modified: 22 Aug 2004                     *
 * Prev Modified: 24 Aug 2004                     *
 * Prev Modified: 25 Aug 2004                     *
 * Prev Modified: 26 Aug 2004                     *
 * Prev Modified: 29 Aug 2004                     *
 * Prev Modified: 15 Sep 2004                     *
 * Prev Modified: 27 Oct 2007                     *
 * Last Modified: 10 Nov 2010                     *
 **************************************************/

#include "qusage.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <ctype.h>

static int usage ( const char * errmsg , ... );
static int version ( void );

/*
** Global opt structure
*/
opt_t opt = {
	0 , 0 , 0 ,	// debug, quiet, verbose
	NULL , NULL ,	// files
	NULL , NULL ,	// user, group
	NULL , 0 ,	// queue, queue_invert_match
	SHOW_DFLT ,	// show
	USE_WALL ,	// cput|wallt
	0 , DIRECT ,	// sorting
	-1 ,		// spacer
	0		// use_stdin
 };


#include <stdarg.h>
/********************************************************************
 *_____________________________  USAGE _____________________________*
 ********************************************************************/
static int usage ( const char * errmsg , ... )
{

   if ( errmsg && *errmsg )
   {
      va_list args;
      BELL;
      fprintf( stderr , "*** Usage error: " );
      va_start( args , errmsg );
      vfprintf( stderr , errmsg , args );
      va_end( args );
      putc( '\n' , stderr );
   }

   fprintf( stderr ,
"\n"
"Usage: %s [OPTIONS]  [ PBS_ACCT_FILE... | --stdin ]\n"
"\n"
"Short options:\n"
"     [-u USERNAME|-g GROUPNAME] [-l|-f]\n"
"     [-Q [!]QUEUE]\n"
"     [-t] [-s] [-a]  [-c|-w]\n"
"     [-J|-C|-A|-U|-G] [-R]\n"
"     [-S SPACER]\n"
"     [-h] [-V] [-d] [-q] [-v]\n"
"\n"
"Long options:\n"
"     [--user USERNAME|--group GROUPNAME] [--show-only-users|--show-only-groups]\n"
"     [--queue [!]QUEUE]\n"
"     [--show-total] [--show-stats] [--show-all]  [--use-cputime|--use-walltime]\n"
"     [--sort-jobs|--sort-cput|--sort-acput|--sort-user|--sort-group] [--reverse]\n"
"     [--spacer SPACER]\n"
"     [--help] [--version] [--debug] [--quiet] [--verbose]\n"
"\n"
, PROGRAM_NAME );

   return ( errmsg ? -1 : 0 );

} /* usage */



/*******************************************************************
 *_____________________________  HELP _____________________________*
 *******************************************************************/
static int help ( void )
{

   fprintf( stdout ,
"\n"
"Usage: %s [OPTIONS]  [ PBS_ACCT_FILE... | --stdin ]\n"
"\n"
"  Selection options:\n"
"      -u USERNAME | --user USERNAME	show only user USERNAME\n"
"      -g GROUPNAME | --group GROUPNAME	show only group GROUPNAME\n"
"      -Q [!]QUEUE | --queue [!]QUEUE	filter input by QUEUE ('!' inverts the match)\n"
"      -l | --show-only-users		show only users table\n"
"      -f | --show-only-groups		show only groups table\n"
"      -t | --show-total			show total\n"
"      -s | --show-stats			show additional statistics data\n"
"      -a | --show-all			show all (total, stats)\n"
"  Sorting options:\n"
"      -J | --sort-jobs			sort by jobs number\n"
"      -C | --sort-cput			sort by cputime\n"
"      -A | --sort-acput			sort by nodes*cputime\n"
"      -U | --sort-user			sort by username (default)\n"
"      -G | --sort-group			sort by groupname\n"
"      -R | --reverse			reverse sorting\n"
"  Misc options:\n"
"      -c | --use-cputime		use cputime instead of walltime\n"
"      -w | --use-walltime		use walltime (default)\n"
"      -S SPACER | --spacer SPACER	use SPACER as fields separator\n"
"					(\"-S ':' -q --show-only-users\")\n"
"      -h | --help			show this message\n"
"      -V | --version			show program version\n"
"      -d | --debug			enable debug output\n"
"      -q | --quiet			only results (no table,warning,...)\n"
"      -v | --verbose			enable progress messages\n"
"\n"
"      -i | --stdin			use stdin instead of PBS_ACCT_FILE(s)\n"
"\n"
"NOTEs:\n"
".   -u implies --show-only-users (groups and total cannot be displayed\n"
".   -g DOES NOT imply --show-only-groups (but total cannot be displayed)\n"
".   PBS_ACCT_FILE = (e.g.) /var/spool/pbs/server_priv/accounting/20040418\n"
".   In order to use special chars as SPACER, try one of these (bash):\n"
"           -S $'\\0'\n"
"           -S \"$(echo -ne \"\\0\")\"\n"
"           -S \"`echo -ne \"\\0\"`\"\n"
"\n"
, PROGRAM_NAME );

   return 0;

} /* help */



/**********************************************************************
 *_____________________________  VERSION _____________________________*
 **********************************************************************/
static int version ( void )
{
   fprintf( stderr , "%s version %s\n" , PROGRAM_NAME , VERSION );
   return 0;
} /* version */



/**********************************************************************
 *_____________________________  OPTIONS _____________________________*
 **********************************************************************/
int options ( int opt_argc , char ** opt_argv )
{

    int c;

    while ( 1 )
    {
       int option_index = 0;

/* struct option { char *name; int has_arg; int *flag; int val; } */
       static struct option long_options[] = {
           { "help"    , 0 , 0 , 'h' },
           { "version" , 0 , 0 , 'V' },
           { "debug"   , 0 , 0 , 'd' },
           { "quiet"   , 0 , 0 , 'q' },
           { "verbose" , 0 , 0 , 'v' },

           { "user"    , 1 , 0 , 'u' },
           { "group"   , 1 , 0 , 'g' },

           { "queue"   , 1 , 0 , 'Q' },

           { "sort-cput"   , 0 , 0 , 'C' },
           { "sort-acput"  , 0 , 0 , 'A' },
           { "sort-jobs"   , 0 , 0 , 'J' },
           { "sort-user"   , 0 , 0 , 'U' },
           { "sort-group"  , 0 , 0 , 'G' },
           { "reverse"     , 0 , 0 , 'R' },

           { "use-cputime"   , 0 , 0 , 'c' },
           { "use-walltime"  , 0 , 0 , 'w' },

           { "show-only-groups"  , 0 , 0 , 'f' }, // f... fellowship?
           { "show-only-users"   , 0 , 0 , 'l' }, // l... lusers?

           { "show-total"  , 0 , 0 , 't' },
           { "show-stats"  , 0 , 0 , 's' },
           { "show-all"    , 0 , 0 , 'a' },

           { "spacer"   , 1 , 0 , 'S' },

           { "stdin"   , 1 , 0 , 'i' },

           { 0 , 0 , 0 , 0 }
       };

       c = getopt_long( opt_argc , opt_argv ,
                        "hVdqv" "u:g:" "Q:" "CAJUGR" "cw" "lf" "tsa" "S:" "i" ,
                        long_options , &option_index );

       if ( c == -1 )
         break;

       switch ( c )
       {
          case 'h': exit( help() );
          case 'V': exit( version() );
          case 'd': opt.debug = 1;   break;
          case 'q': opt.quiet = 1;   break;
          case 'v': opt.verbose = 1; break;

          case 'u':
            if ( opt.user )
              return usage( "user defined twice" );
            if ( opt.group )
              return usage( "both user and group defined" );
            opt.user = optarg;
            break;
          case 'g':
            if ( opt.group )
              return usage( "group defined twice" );
            if ( opt.user )
              return usage( "both user and group defined" );
            opt.group = optarg;
            break;

          case 'Q':
            if ( opt.queue )
              return usage( "queue defined twice" );
            opt.queue_invert_match = ( *optarg == '!' );
            opt.queue = (optarg + opt.queue_invert_match);
            break;

          case 'C':
          case 'A':
          case 'J':
          case 'U':
          case 'G':
            if ( opt.sort )
              return usage( "sort defined twice" );
            opt.sort = c;
            break;

          case 'R':
            if ( opt.order )
              return usage( "order defined twice" );
            opt.order = REVERSE;
            break;

          case 'c':
          case 'w':
            opt.use_cput = ( c == 'c' ) ? USE_CPUT : USE_WALL;
            break;

          case 'f':
            if ( SHOW_ONLY( USERS ) )
              return usage( "show-only-users and show-only-groups both defined" );
            HIDE( USERS ) , SHOW( GROUPS );
            break;
          case 'l':
            if ( SHOW_ONLY( GROUPS ) )
              return usage( "show-only-users and show-only-groups both defined" );
            HIDE( GROUPS ) , SHOW( USERS );
            break;

          case 't': SHOW( TOTAL ); break;
          case 's': SHOW( STATS ); break;
          case 'a': SHOW( TOTAL ) , SHOW( STATS ); break;

          case 'S':
            if ( opt.spacer >= 0 )
              return usage( "spacer already defined: %d" , opt.spacer );
/*
** I want to preserve this:
**		./qusage accounting/2004* -S "$(echo -e "\0")"
** here optarg is != NULL, otherwise getopt should be already gone.
//            if ( optarg && *optarg && *(optarg+1) )
*/
            if ( *optarg && *(optarg+1) )
              return usage( "spacer must be one char long" );
            opt.spacer = (short)(*optarg);
            break;

          case 'i': opt.use_stdin = 1 ; break;

          case '?':
            /* getopt_long already print an error message (unless opterr=1) */
            BELL;
            return usage( "" );	// "" is not NULL

          default:
            BELL;
            fprintf( stderr , "?? getopt returned character code 0%o [%c] ??\n"
                            , c , ( isprint( c ) ? c : ' ' )
                   );
            abort ();
       }
    }

/*
** sanity checks
*/

    if ( opt.user && SHOW_ONLY( GROUPS ) )
      return usage( "user given but show-only-groups requested" );

    if ( opt.user )
    {
       if ( ISSHOW( TOTAL ) )
       {
          BELL;
          NQprint( "WARNING: user defined, groups and total cannot be displayed"
                   " because users!=user are not considered\n" );
       } // GROUPS silently hidden
       HIDE( GROUPS ) , HIDE( TOTAL ) , SHOW( USERS );
    }

    if ( opt.group && ISSHOW( TOTAL ) )
    {
       BELL;
       NQprint( "WARNING: group defined, total cannot be displayed"
                " because groups!=group are not considered\n" );
       HIDE( TOTAL ) , SHOW( GROUPS );
    }

    if ( opt.debug )
    {
       fprintf( stderr ,
                "Options:\n"
                "\toptind             %d\n"
                "\tint debug          %d\n"
                "\tint quiet          %d\n"
                "\tint verbose        %d\n"
                "\tchar * user        %s\n"
                "\tchar * group       %s\n"
                "\tchar * queue       %s\n"
                "\tint show           0x%02X,%s%s%s%s%s\n"
                "\tint use_cput       %d, %s\n"
                "\tchar sort          %c\n"
                "\tint order          %d, %s\n"
                , optind
                , opt.debug , opt.quiet , opt.verbose
                , opt.user , opt.group
                , opt.queue
                , opt.show
                , ISSHOW( USERS  ) ? " USERS"  : ""
                , ISSHOW( GROUPS ) ? " GROUPS" : ""
                , ISSHOW( TOTAL  ) ? " TOTAL"  : ""
                , ISSHOW( STATS  ) ? " STATS"  : ""
                , SHOW_EQ( ALL )   ? " (ALL)"  : ""
                , opt.use_cput , opt.use_cput ? "USE CPUTIME" : "USE WALLTIME"
                , opt.sort ? opt.sort : '-'
                , opt.order , opt.order ? "REVERSE" : "DIRECT"
              );
       if ( opt.spacer != -1 )
         fprintf( stderr , "\tchar spacer        0x%02d, %c\n"
                         , opt.spacer , ( isprint( opt.spacer ) ? opt.spacer : '?' )
                );
       fprintf( stderr , "\n" );
    }

    if ( opt.use_stdin )
    {
       if ( optind < opt_argc )
         fprintf( stderr , "WARNING: --stdin and input files given, ignoring input files\n" );
       return 0;
    }

    if ( optind >= opt_argc )	// no files
      return usage( "no file(s) given" );

    opt.first_file = ( opt_argv + optind ) ;
    opt.last_file  = ( opt_argv + opt_argc -1 ) ;

    if ( opt.debug )
    {
       fprintf( stderr ,
                "\tchar ** first_file %s\n"
                "\tchar ** last_file  %s\n"
                "\n"
                , (opt.first_file)?*opt.first_file:"-"
                , (opt.last_file)?*opt.last_file:"-"
              );
       {
          int i = 0;
          char ** f = opt.first_file;
          fprintf( stderr ,
                   "\t%d files to parse:\n"
                   , opt_argc - optind
                 );
          while( f <= opt.last_file )
            printf( "\t\tfile%d '%s'\n" , ++i , *(f++) );
          fprintf( stderr , "\n" );
       }
    }

    return 0;

} /* options */



/***********************  E N D   O F   F I L E  ************************/

