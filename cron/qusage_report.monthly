#!/bin/bash

# Copyright (C) 2004-2011  Moreno 'baro' Baricevic
#                                       <baro AT democritos DOT it>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Path: /etc/cron.monthly             #
# File: qusage_report.monthly         #
# Date: 04 Aug 2004                   #
#-------------------------------------#
# Last Modified: 04 Mar 2009          #
#=====================================#
#
# Checks and reports about the previous month resources usage.
# The e-mail is sent to root from crond user.
# This script is supposed to be executed the 1st day of the month by cron:
#	35 01 1 * * root /path_to_this_script/qusage_report.monthly
# (each 1st day at 1:35 AM)
# or just place it in /etc/cron.monthly/
#
# Adjust QUSAGE and PBSACCTDIR accordingly to your installation paths.
#

test -z "$QUSAGE"	&& QUSAGE=/usr/local/sbin/qusage
test -z "$PBSACCTDIR"	&& PBSACCTDIR=/var/spool/torque/server_priv/accounting

lastmonth="$(date -d "1 month ago" +%Y%m)";

test -t 0 && INTERACTIVE=true || INTERACTIVE=false

function send()
{
	if ! $INTERACTIVE
	then
		host=`hostname`
		mail -s "PBS accounting statistics for ${host} [${lastmonth}*]" root@localhost
	else
		cat -
	fi
}

$QUSAGE -a $PBSACCTDIR/${lastmonth}* | send

#EOF
